var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var passportLocalMongoose = require('passport-local-mongoose');

var Caronas = new Schema( {
	motorista: { type: String, required: true },
	users: [String]
});

Caronas.plugin(passportLocalMongoose);

module.exports = mongoose.model ('Caronas',Caronas);
