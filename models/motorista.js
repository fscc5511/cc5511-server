var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var passportLocalMongoose = require('passport-local-mongoose');

var Motorista = new Schema( {
	modelo: String,
	placa: String,
	cor: String,
	origem: { type: String, required: true },
	destino: { type: String, required: true },
	pagamento: String,
	fumante: String,
	IV: String,
	email: { type: String, required: true },
	celular: { type: String, required: true },
	username: { type: String, required: true, index: { unique: true } },
	nome: { type: String, required: true }
});

Motorista.plugin(passportLocalMongoose);

module.exports = mongoose.model ('Motorista',Motorista);
