var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var passportLocalMongoose = require('passport-local-mongoose');

var Account = new Schema({
	username: { type: String, required: true, index: { unique: true } },
    password: { type: String, required: true },
	email: { type: String, required: true, index: { unique: true } },
	nome: { type: String, required: true },
	sobrenome: String,
	endereco: String,
	celular: { type: String, required: true },
	sexo: String,
	gosto: String
});

Account.plugin(passportLocalMongoose);

module.exports = mongoose.model('Account', Account);
