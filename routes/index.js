var express = require('express');
var router = express.Router();
var passport = require('passport');
var Motorista = require('../models/motorista');
var Account = require('../models/account');
var nodemailer = require('nodemailer');

var bCrypt = require('bcrypt');

var name = 'Sistema de caronas FEI';

var login = '';
var conta = null;

var createHash = function(password){
 return bCrypt.hashSync(password, bCrypt.genSaltSync(10), null);
}

var iv = function(checkbox){
	if (checkbox==="on"){
		return "Sim";
	}
	return "Não";
}

/* GET home page. */
router.get('/', function(req, res, next) {
  	res.render('index', { title: name, usuarioLogado : login});
});

router.get('/sobre', function(req, res, next) {
  res.render('sobre', { title: name, usuarioLogado : login });
});

router.get('/logon', function(req, res, next) {
  res.render('logon', { title: name, usuarioLogado : login });
});


router.get('/carona', function(req, res, next) {
	var account = null;

	Account.findOne({ 'username' :  req.body.username },
	  function(err, user) {
		if (err)
		  res.redirect('/logon');
		if (user){
			account = user;
		}
	  }
	);

	Motorista.find({}, function(err, docs) {
		if (err){
			console.log("ERRO CARONA!");
			res.redirect('/');
		}

		if(login!==''){
			res.render('carona', { title: name, usuarioLogado : login, data: docs});
		}
	  	else{
			res.redirect('/');
		}
	});
});

router.get('/register', function(req, res) {
    res.render('register', { title: name, usuarioLogado : login, exists: false });
});

router.post('/register', function(req, res) {
    Account.register(new Account({ 	username: req.body.username,
									password: createHash(req.body.password),
								   	email: req.body.email,
								 	nome: req.body.nome,
									sobrenome: req.body.sobrenome,
									endereco: req.body.endereco,
									celular: req.body.celular,
									sexo: req.body.optradio,
									gosto: req.body.gosto}),
									req.body.password, function(err, account) {

        if (err) {
            console.log(err);
            return res.render('register', { account : account, title: name, usuarioLogado : login, exists: true });
        }

        passport.authenticate('local')(req, res, function () {
			conta = account;
			login = req.body.username;
            res.render('index', { title: name, usuarioLogado : login});
        });
    });
});

router.post('/removemotorista', function(req, res) {
    Motorista.findOne({ 'username' : login },
	  function(err, data) {
		if (err || !data)
		    res.render('removemotorista', { title: name, usuarioLogado : login, exists: false});
		if (data){
            Motorista.remove({'username': login}, function(err) {
        		if (err){
        			console.log(err);
        		}
        	});
			res.render('removemotorista', { title: name, usuarioLogado : login, exists: true});
		}
	  }
	);
    // if(login!==''){
    //
    // }
    // else{
    //     res.redirect('/');
    // }
	
});

router.get('/motorista', function(req, res) {
	if(login!==''){
		res.render('motorista', { title: name, usuarioLogado : login, exists: false });
	}
  	else{
		res.redirect('/');
	}
});

router.post('/motorista', function(req, res) {
	if(login==''){
		res.redirect('/');
	}
  	else{

		Account.findOne({ 'username' :  login },
		  function(err, user) {
			if (err)
			  res.redirect('/');
			if (user){
				Motorista.register(new Motorista({username: login,
	  	  										modelo: req.body.modelo,
	  	  									   	placa: req.body.placa,
	  	  									 	cor: req.body.cor,
	  	  										origem: req.body.origem,
	  	  										destino: req.body.destino,
	  	  										pagamento: req.body.pagamento,
	  	  	                                    fumante: req.body.fumante,
												IV: iv(req.body.iv),
												email: user.email,
												celular: user.celular,
	  	  	                                    nome: user.nome}),
	  	  	                  'gerahash',function(err, account) {
	  	  	        if (err) {
	  	  	            console.log(err+" "+login);
	  	  	            return res.render('motorista', { account : account, title: name, usuarioLogado : login, exists: true });
	  	  	        }

	  	  	        // passport.authenticate('local')(req, res, function () {
	  	  	            res.redirect('/');
	  	  	        // });
  	  	    	});
			}
		  }
		);
	}
});

router.get('/home', function(req, res) {
	if(login!==''){
		res.render('home', { title: name, usuarioLogado : login, email: conta.email,
																	nome: conta.nome,
																	sobrenome: conta.sobrenome,
																	endereco: conta.endereco,
																	celular: conta.celular,
																	sexo: conta.sexo,
																	gosto: conta.gosto });
	}
  	else{
		res.redirect('/');
	}
});
router.get('/atualiza', function(req, res) {
    if(login!==''){
		res.render('atualiza', { title: name, usuarioLogado : login});
	}
  	else{
		res.redirect('/');
	}
});
router.post('/atualiza', function(req, res) {
	Account.findOne({ 'username' :  login },
	  function(err, user) {
		if (err)
		  res.redirect('/');
		if (user){
			if(req.body.password !== "") user.password = createHash(req.body.password);
		   	if(req.body.email !== "") user.email = req.body.email;
		 	if(req.body.nome!== "") user.nome = req.body.nome;
			if(req.body.sobrenome!== "") user.sobrenome = req.body.sobrenome;
			if(req.body.endereco!== "") user.endereco = req.body.endereco;
			if(req.body.celular!== "") user.celular = req.body.celular;
			if(req.body.optradio !== undefined) user.sexo = req.body.optradio;
			if(req.body.gosto!== "") user.gosto = req.body.gosto;
			user.save();
			conta = user;

			Motorista.find({'username': login}, function(err, motora) {
				if (err){
					console.log("ERRO HOME!");
					res.redirect('/home');
				}
				motora.nome = user.nome;
				motora.email = user.email;
				motora.celular = user.celular;
				// motora.save();
			});
		}
	  });

	  res.redirect("/home");
});

router.get('/forgotten', function(req, res) {
    res.render('forgotten', { title: name, usuarioLogado : login, emailfalse: false });
});

router.post('/forgotten', function(req, res) {

    var responseuser = res;

	Account.findOne({ 'email' :  req.body.email },
      function(err, user, res) {

        if (err){
		  console.log(err);
          responseuser.render('forgotten', { title: name, usuarioLogado : login, emailfalse: true });
	  	}

        if (!user){
          console.log('Email não encontrado '+ req.body.email);
          responseuser.render('forgotten', { title: name, usuarioLogado : login, emailfalse: true });
        }

        if (user){

    		var smtpTransport = nodemailer.createTransport("SMTP",{
    		   service: "Gmail",
    		   auth: {
    		       user: "sistemadecaronasfei@gmail.com",
    		       pass: "sistemacc5511"
    		   }
    		});
            random = (Math.random()).toString(36);
            Account.findOne({ 'email' :  req.body.email },
        	  function(err, user) {
                  user.password = createHash(random);
                  user.save()
            });
    		smtpTransport.sendMail({
    		   from: "Sistema de caronas FEI <sistemadecaronasfei@gmail.com>", // sender address
    		   to: req.body.email, // comma separated list of receivers
    		   subject: "Sistema de caronas FEI - Senha", // Subject line
    		   text: "Olá, "+user.username+". Sua senha é: "+random // plaintext body
    		}, function(error, response){
    		   if(error){
    		       console.log(error);
    		   }else{
    		       console.log("Message sent: " + response.message);
    		   }
    		});
            responseuser.redirect('/');
        }
	});

});

router.get('/login', function(req, res) {
    res.redirect('/');
});

router.post('/login', passport.authenticate('login', { failureRedirect: '/logon'}), function(req, res) {
	Account.findOne({ 'username' :  req.body.username },
	  function(err, user) {
		if (err)
		  res.redirect('/logon');
		if (user){
			conta = user;
		}
	  }
	);
	login = req.body.username;
	res.redirect('/');
});

router.get('/logout', function(req, res) {
	login = '';
    req.logout();
    res.redirect('/');
});

module.exports = router;
