FROM node
RUN npm install -g bower
ADD package.json /server/
ADD bower.json /server/
ADD .bowerrc /server/
WORKDIR /server/
RUN npm install
RUN bower install --allow-root
ADD . /server/
CMD ["npm", "start"]